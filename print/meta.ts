
import { makeRecord } from 'sdi/source';
import { fromRecord } from 'sdi/locale';

export const attribution = () => fromRecord(makeRecord("Relais d'Action de Quartier", "Buurt Actie Relais"))

export const credits = () => fromRecord(makeRecord(
    'Fond de plan: Brussels UrbIS ®© - CIRB - CIBG',
    'Achtergrond: Brussels UrbIS ®© - CIRB - CIBG'))