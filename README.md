This is a specific theme for _cartostation_, which follows design rules for _perspective.brussels_.

**This theme folder as to be at the same level as _cartostation_ folder.**

## Core style definitions

It uses a collection of core style definitions that are defined in `cartostation/client/platform/core`

Core style definitions are mainly about interface orgnization and structure.

`core` and `var` definitions are defined in `/style/sdi.less`
